FROM debian:9
RUN apt-get update -yq \
&& apt-get install curl gnupg -yq \
&& curl -sL https://deb.nodesource.com/setup_10.x | bash \
&& apt-get install nodejs -yq \
&& apt-get clean -y

RUN npm install -g http-server
WORKDIR /app
COPY app/package*.json ./
RUN npm install
COPY app .

RUN npm run build

EXPOSE 8080

CMD [ "http-server", "dist" ]